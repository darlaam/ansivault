# ansivault

Ansible vault 1.1 rust implementation,
While not totally feature complete, ansivault allow to work with or without ansible headers.

**Features:**
* *encrypt* a string or file (with or without ansible header)
* *decrypt* a string or file (with or without ansible header)
* *rekey* a string or file (plain encrypted, with or without ansible header)
* *view* a file (same as decrypt it to stdout)
Input and output can be from argument, file or stdin/stdout

> When fiddling with vault file and key provided by arguments, be careful of the *EOL* problem…  
> Editors tends to add an *EOL* (end of line) character after the last char. Which will not be present
> if you provide the key with the `k` option.
> In *vim*, you can avoid this behaviour with `:set noendofline nofixendofline`

## Examples
```bash
# Encrypt from arg
ansivault -a -k toto encrypt -s lorem ipsum
$ANSIBLE_VAULT;1.1;AES256
      62393433633962666237643534326531633166353364646339623837363231343131656662323465
      3731396538376632623432336166643835663538363234340a303735396438636266306538633637
      32613133303933396563386439366464386133363666616262633331626165333164313566376561
      3861353538663538610a393965653462383031303334623032636336333831663364613234316430
      6266

# Encrypt raw (not ansible)
ansivault -k toto encrypt -s lorem ipsum
636237313635343038393061303962363332616538343638623938643531653064643339353434326137383134613338336133626165656165316335343034610a616233626238316331613966613065363161646637393936613339656666633736643334396538616537653637616165343338613132663838303832653739610a3439643933653237346632646335323663353437623839353637393537383963%

# Roundtrip from stdin / stdout
echo "lorem ipsum" | ansivault -ak toto encrypt | ansivault -ak toto decrypt
lorem ipsum

# Encrypt from file to file
ansivault -ak toto -o my_output_file input_file
```

## Configuration
In order to avoid giving the vault key as an argument, an environment variable `ANSIVAULT_KEY_FILE`
can be set to point to the file containing the vault key.

## Usage
```bash
ansivault [options] <command> [command options] [argument]
```
### Common options
* *h, help*:    display help message and exit
* *v, verbose*: log level, default to vvv (INFO)
* *q, quiet*:   disable all logs
* *V, version*: display version and exit
* *a, ansible*: expect ansible headers (1.1) to be present if provided
* *k, key*:     vault key to use

### Commands
#### decrypt
Use to decrypt a file or a given string. Input cannot contain mixed data (file with clear text
and encrypted values, for example). If `ansible` flag is not provided, operation will fail if the input
has the ansible vault header.
**Usage**
```bash
ansivault [common options] decrypt [hios] [INPUT_FILE]
```
* *h, help*: display help on decrypt command
* *o, output OUTPUT_FILE*: output file for decrypted data. Default to stdout if not provided
* *s, input-string INPUT_STRING*: a string to ben encoded. If provided, INPUT_FILE will be ignored
* *INPUT_FILE*: the path to the file to decrypt. If not provided, stdin is used

#### encrypt
Use to encrypt a file or a given string. If the `ansible` flag is provided, ansible vault header
will be present in the output.

**Usage**
```bash
ansivault [common options] encrypt [hios] [INPUT_FILE]
```
* *h, help*: display help on encrypt command
* *o, output OUTPUT_FILE*: output file for encrypted data. Default to stdout if not provided
* *s, input-string INPUT_STRING*: a string to ben encoded. If provided, INPUT_FILE will be ignored
* *INPUT_FILE*: the path to the file to encrypt. If not provided, stdin is used

#### rekey
Rekey a file with the provide new key. The file will be rekeyed in-place.

**Usage**
```bash
ansivault [common options] rekey [h]n  INPUT_FILE
```
* *h, help*: display help on rekey command
* *n, new-key NEW_KEY*: the new key to rekey with
* *INPUT_FILE*: the path to the file to encrypt

#### view
Decrypt and print an encrypted file.
Same behaviour as `ansible decrypt INPUT_FILE`

**Usage**
```bash
ansivault [common options] view [h] INPUT_FILE
```
* *h, help*: display help on view command
* *INPUT_FILE*: the path to the file to display

## License

This project is licensed under either of

 * [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
 * [MIT license](http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in guidon by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

----
Doc generated with `cargo readme`
