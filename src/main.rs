//! Ansible vault 1.1 rust implementation,
//! While not totally feature complete, ansivault allow to work with or without ansible headers.
//!
//! **Features:**
//! * *encrypt* a string or file (with or without ansible header)
//! * *decrypt* a string or file (with or without ansible header)
//! * *rekey* a string or file (plain encrypted, with or without ansible header)
//! * *view* a file (same as decrypt it to stdout)
//! Input and output can be from argument, file or stdin/stdout
//!
//! # Examples
//! ```bash
//! # Encrypt from arg
//! ansivault -a -k toto encrypt -s lorem ipsum
//! $ANSIBLE_VAULT;1.1;AES256
//!       62393433633962666237643534326531633166353364646339623837363231343131656662323465
//!       3731396538376632623432336166643835663538363234340a303735396438636266306538633637
//!       32613133303933396563386439366464386133363666616262633331626165333164313566376561
//!       3861353538663538610a393965653462383031303334623032636336333831663364613234316430
//!       6266
//!
//! # Encrypt raw (not ansible)
//! ansivault -k toto encrypt -s lorem ipsum
//! 636237313635343038393061303962363332616538343638623938643531653064643339353434326137383134613338336133626165656165316335343034610a616233626238316331613966613065363161646637393936613339656666633736643334396538616537653637616165343338613132663838303832653739610a3439643933653237346632646335323663353437623839353637393537383963%
//!
//! # Roundtrip from stdin / stdout
//! echo "lorem ipsum" | ansivault -ak toto encrypt | ansivault -ak toto decrypt
//! lorem ipsum
//!
//! # Encrypt from file to file
//! ansivault -ak toto -o my_output_file input_file
//! ```
//!
//! # Configuration
//! In order to avoid giving the vault key as an argument, an environment variable `ANSIVAULT_KEY_FILE`
//! can be set to point to the file containing the vault key.
//!
//! # Usage
//! ```bash
//! ansivault [options] <command> [command options] [argument]
//! ```
//! ## Common options
//! * *h, help*:    display help message and exit
//! * *v, verbose*: log level, default to vvv (INFO)
//! * *q, quiet*:   disable all logs
//! * *V, version*: display version and exit
//! * *a, ansible*: expect ansible headers (1.1) to be present if provided
//! * *k, key*:     vault key to use
//!
//! ## Commands
//! ### decrypt
//! Use to decrypt a file or a given string. Input cannot contain mixed data (file with clear text
//! and encrypted values, for example). If `ansible` flag is not provided, operation will fail if the input
//! has the ansible vault header.
//! **Usage**
//! ```bash
//! ansivault [common options] decrypt [hios] [INPUT_FILE]
//! ```
//! * *h, help*: display help on decrypt command
//! * *o, output OUTPUT_FILE*: output file for decrypted data. Default to stdout if not provided
//! * *s, input-string INPUT_STRING*: a string to ben encoded. If provided, INPUT_FILE will be ignored
//! * *INPUT_FILE*: the path to the file to decrypt. If not provided, stdin is used
//!
//! ### encrypt
//! Use to encrypt a file or a given string. If the `ansible` flag is provided, ansible vault header
//! will be present in the output.
//!
//! **Usage**
//! ```bash
//! ansivault [common options] encrypt [hios] [INPUT_FILE]
//! ```
//! * *h, help*: display help on encrypt command
//! * *o, output OUTPUT_FILE*: output file for encrypted data. Default to stdout if not provided
//! * *s, input-string INPUT_STRING*: a string to ben encoded. If provided, INPUT_FILE will be ignored
//! * *INPUT_FILE*: the path to the file to encrypt. If not provided, stdin is used
//!
//! ### rekey
//! Rekey a file with the provide new key. The file will be rekeyed in-place.
//!
//! **Usage**
//! ```bash
//! ansivault [common options] rekey [h]n  INPUT_FILE
//! ```
//! * *h, help*: display help on rekey command
//! * *n, new-key NEW_KEY*: the new key to rekey with
//! * *INPUT_FILE*: the path to the file to encrypt
//!
//! ### view
//! Decrypt and print an encrypted file.
//! Same behaviour as `ansible decrypt INPUT_FILE`
//!
//! **Usage**
//! ```bash
//! ansivault [common options] view [h] INPUT_FILE
//! ```
//! * *h, help*: display help on view command
//! * *INPUT_FILE*: the path to the file to display

use std::fs::File;
use std::io::{stdin, Read, Write};
use std::process::exit;

use anyhow::{bail, Context, Result};
use log::{error, info, trace};

use crate::cli_opts::{
    CliOpts, CommandOpts, DecryptOpts, EncryptOpts, OptionsExt, RekeyOpts, ViewOpts,
};

mod cli_opts;

const VERSION: &str = env!("CARGO_PKG_VERSION");
const AVC_KEY_FILE: &str = "ANSIVAULT_KEY_FILE";

fn main() {
    if let Err(e) = vault_main() {
        error!("{}", e.to_string());
        exit(1);
    }
}

fn vault_main() -> Result<()> {
    // Get opts
    let opts = CliOpts::parse_args_default_or_errror();
    if let Err(e) = opts.as_ref() {
        eprintln!("Bad arguments: {}", e.to_string());
        exit(2);
    }
    let opts = opts.unwrap();

    // If version, display it and quit
    if opts.version {
        println!("AVC {}", VERSION);
        return Ok(());
    }
    // Init log
    init_logs(opts.verbose, opts.quiet)?;
    info!("AVC {}", VERSION);

    if opts.command.is_none() {
        bail!("No command provided !");
    }

    // Retrieve key
    let key = match opts.key {
        Some(k) => k,
        None => {
            let key_file =
                std::env::var(AVC_KEY_FILE).with_context(|| "No key provided ($ANSIVAULT_KEY_FILE ?)")?;
            let mut rkey = String::new();
            File::open(&key_file)
                .with_context(|| format!("Key file not found: {}", &key_file))?
                .read_to_string(&mut rkey)?;
            // We don't want EOL char at the end of the file.
            if rkey.ends_with("\n") {
                rkey.pop();
                if rkey.ends_with("\r") {
                    rkey.pop();
                }
            }
            rkey
        }
    };
    trace!("Retrieved key: {}", key);

    match opts.command {
        Some(CommandOpts::Decrypt(d)) => avc_decrypt(d, &key, opts.ansible)?,
        Some(CommandOpts::Encrypt(e)) => avc_encrypt(e, &key, opts.ansible)?,
        Some(CommandOpts::Rekey(r)) => avc_rekey(r, &key, opts.ansible)?,
        Some(CommandOpts::View(v)) => avc_view(v, &key, opts.ansible)?,
        None => trace!("Nothing to do"),
    }

    Ok(())
}

fn init_logs(verbosity: usize, quiet: bool) -> Result<()> {
    let verbosity = if verbosity == 0 { 3 } else { verbosity };
    if let Err(e) = stderrlog::new()
        .module(module_path!())
        .quiet(quiet)
        .verbosity(verbosity)
        .init()
    {
        eprint!("Log initialization failed - {}", e.to_string());
        bail!(e)
    }
    Ok(())
}

fn avc_decrypt(opts: DecryptOpts, key: &str, ansible_mode: bool) -> Result<()> {
    let reader: Box<dyn Read> = match &opts.input_string {
        Some(s) => Box::new(s.as_bytes()),
        None => {
            if let Some(path) = &opts.input {
                Box::new(File::open(path).with_context(|| {
                    format!("Failed to read file {}", path.display().to_string())
                })?)
            } else {
                println!("ctrl+D to end input");
                Box::new(stdin())
            }
        }
    };

    let decrypted = if ansible_mode {
        ansible_vault::decrypt_vault(reader, key)?
    } else {
        ansible_vault::decrypt(reader, key)?
    };

    if let Some(path) = opts.output {
        let mut f = File::open(path)?;
        f.write_all(decrypted.as_slice())?;
    } else {
        print!("{}", String::from_utf8(decrypted)?);
    }

    Ok(())
}

fn avc_encrypt(opts: EncryptOpts, key: &str, ansible_mode: bool) -> Result<()> {
    let reader: Box<dyn Read> = match &opts.input_string {
        Some(s) => Box::new(s.as_bytes()),
        None => {
            if let Some(path) = &opts.input {
                Box::new(File::open(path).with_context(|| {
                    format!("Failed to read file {}", path.display().to_string())
                })?)
            } else {
                println!("ctrl+D to end input");
                Box::new(stdin())
            }
        }
    };

    let encrypted = if ansible_mode {
        ansible_vault::encrypt_vault(reader, key)?
    } else {
        ansible_vault::encrypt(reader, key)?
    };

    if let Some(path) = opts.output {
        let mut f = File::open(path)?;
        f.write_all(encrypted.as_bytes())?;
    } else {
        print!("{}", encrypted);
    }

    Ok(())
}

fn avc_rekey(opts: RekeyOpts, key: &str, ansible_mode: bool) -> Result<()> {
    let mut f = File::open(opts.input)?;

    let decrypted = if ansible_mode {
        ansible_vault::decrypt_vault(&f, key)?
    } else {
        ansible_vault::decrypt(&f, key)?
    };
    let encrypted = if ansible_mode {
        ansible_vault::encrypt_vault(decrypted.as_slice(), key)?
    } else {
        ansible_vault::encrypt(decrypted.as_slice(), key)?
    };

    f.set_len(0)?;
    f.write_all(encrypted.as_bytes())?;
    Ok(())
}

fn avc_view(opts: ViewOpts, key: &str, ansible_mode: bool) -> Result<()> {
    let f = File::open(opts.input)?;

    let decrypted = if ansible_mode {
        ansible_vault::decrypt_vault(f, key)?
    } else {
        ansible_vault::decrypt(f, key)?
    };

    println!("{}", String::from_utf8(decrypted)?);

    Ok(())
}
