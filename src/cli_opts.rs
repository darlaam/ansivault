use std::env::args;
use std::path::PathBuf;
use std::process::exit;

use anyhow::Result;
use gumdrop::{Options, ParsingStyle};

#[derive(Options, Debug)]
pub(crate) struct CliOpts {
    #[options(help = "Print help message")]
    pub help: bool,
    #[options(count, help = "Log level. Default INFO")]
    pub verbose: usize,
    #[options(help = "No logs")]
    pub quiet: bool,
    #[options(help = "Display version and quit", short = "V")]
    pub version: bool,
    #[options(help = "Ansible mode")]
    pub ansible: bool,
    #[options(help = "Configuration key")]
    pub key: Option<String>,
    #[options(command)]
    pub command: Option<CommandOpts>,
}

#[derive(Options, Debug)]
pub enum CommandOpts {
    Decrypt(DecryptOpts),
    Encrypt(EncryptOpts),
    View(ViewOpts),
    Rekey(RekeyOpts),
}

#[derive(Options, Debug)]
pub struct DecryptOpts {
    #[options(help = "Print help message")]
    pub help: bool,
    #[options(help = "Output file name. Omit for stdout")]
    pub output: Option<PathBuf>,
    #[options(free, help = "Input file. Omit for stdin")]
    pub input: Option<PathBuf>,
    #[options(
        help = "Input as parameter. If present i option will be ignored",
        short = "s"
    )]
    pub input_string: Option<String>,
}

#[derive(Options, Debug)]
pub struct EncryptOpts {
    #[options(help = "Print help message")]
    pub help: bool,
    #[options(help = "Output file name. Omit for stdout")]
    pub output: Option<PathBuf>,
    #[options(
        help = "Input as parameter. If present i option will be ignored",
        short = "s"
    )]
    pub input_string: Option<String>,
    #[options(free, help = "Input file. Omit for stdin")]
    pub input: Option<PathBuf>,
}

#[derive(Options, Debug)]
pub struct ViewOpts {
    #[options(help = "Print help message")]
    pub help: bool,
    #[options(free, help = "Input file.")]
    pub input: PathBuf,
}

#[derive(Options, Debug)]
pub struct RekeyOpts {
    #[options(help = "Print help message")]
    pub help: bool,
    #[options(help = "New key")]
    pub new_key: String,
    #[options(free, help = "Input file.")]
    pub input: PathBuf,
}

impl<O> OptionsExt for O
where
    O: Options,
{
    fn parse_args_default_or_errror() -> Result<Self>
    where
        Self: Sized,
    {
        let args = args().collect::<Vec<_>>();

        let opts = Self::parse_args(&args[1..], ParsingStyle::default())?;

        if opts.help_requested() {
            let mut command = &opts as &dyn Options;
            let mut command_str = String::new();

            while let Some(new_command) = command.command() {
                command = new_command;

                if let Some(name) = new_command.command_name() {
                    command_str.push(' ');
                    command_str.push_str(name);
                }
            }

            println!("Usage: {}{} [OPTIONS]", args[0], command_str);
            println!();
            println!("{}", command.self_usage());

            if let Some(cmds) = command.self_command_list() {
                println!();
                println!("Available commands:");
                println!("{}", cmds);
            }

            exit(0);
        }

        Ok(opts)
    }
}
pub trait OptionsExt {
    fn parse_args_default_or_errror() -> Result<Self>
    where
        Self: Sized;
}
